

// Examples with Array
const myArrOne = [1, 2, 3];
const myArrTwo = [4, 5, 6];
const combinedArray = [...myArrOne, ...myArrTwo, 7, 8, 9];
console.log(combinedArray); // [1, 2, 3, 4, 5, 6, 7, 8, 9]

const sum = (a, b, c) => a + b + c;
let sumArray = sum(...[1, 2, 3]);
console.log(sumArray); // 6


console.log(...['JavaScript', 'is', 'the', '****']); // JavaScript is the ****


// Examples with objects
const object = {
  x: 1,
  y: 2
};

const newObject = {...object, x: 4 };
console.log(newObject); // { x: 4, y: 2 }

const firstObj = { x: 1 };
const secondObj = { x: 2 };
const thirdObj = { x: 3 };
const finalObj = { ...firstObj, ...secondObj, ...thirdObj };
console.log(finalObj); // { x: 3 }

//
