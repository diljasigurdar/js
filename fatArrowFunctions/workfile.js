

function first() {
  console.log(this); // global
  second();
}

const second = () => {
  this.x = 1;
  console.log(this); // { x: 1 }
  third(this);
}

const third = (self) => {
  this.y = 2;
  console.log(this === self); // true
  fourth();
}

const fourth = () => {
  console.log(this); // { x: 1, y: 2 }
}

first();
