

const x = 1, y = 2, name = 'Diljá Sigurðardóttir';


// The old way
var oldGreeting = 'Hello, ' + name + '. 1 + 2 is ' + (x+y);
console.log(oldGreeting); // Hello, Diljá Sigurðardóttir. 1 + 2 is 3

// The new way
const newGreeting = `Hello, ${name}. 1 + 2 is ${x + y}`;
console.log(newGreeting); // Hello, Diljá Sigurðardóttir. 1 + 2 is 3


// `` --> option+*
// 

// Will result in parsing error
const myOldString = 'Lorem ipsum dolor sit amet,

consectetur adipisicing elit

sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';


// Will work just fine!
const myStinr = `Lorem ipsum dolor sit amet,

consectetur adipisicing elit

sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`;
