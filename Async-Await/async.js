/* PART I - Understanding asynchronous functions */

// first example
function populateArray() {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      resolve([1, 2, 3, 4, 5]);
    }, 1000);
  });
};

// Þetta er ekki async fall þess vegna er listinn tómur
function getArray() {
  var myArr = [];
  populateArray().then(data => {
    myArr = data;
  });
  return myArr;
};

getArray(); // []

// second Example
getNextSequence.initialNumber = 1;

function getNextSequence() {
  return new Promise((resolve, reject) => {
    setTimeout(function() {
      resolve(getNextSequence.initialNumber++)
    }, 1000);
  });
}

// Use getNextSequence to count to four using chained promises
getNextSequence().then(function (number) {
  console.log(number);
  return getNextSequence();
}).then(function (number) {
  console.log(number);
  return getNextSequence();
}).then(function (number) {
  console.log(number);
  return getNextSequence();
}).then(function (number) {
  console.log(number);
  return getNextSequence();
});

// Convert the chained promises to async / await instead
(async function() {
  var one = await getNextSequence();
  var two = await getNextSequence();
  var three = await getNextSequence();
  var four = await getNextSequence();
  console.log(one, two, three, four);
})();


// Convert the async / await to Promise.all


// Use Date() to measure the time for 2. and 3.

























//
