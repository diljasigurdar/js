

// Encapsulating a promise
// Þetta dæmi er ekki gott ef við erum með promise rejection
function processData() {
  return new Promise(function (resolve, reject) {
    server.getData().then(function (success, resp) {
      if (!success) { reject(resp.errorMsg); }
      resolve(resp);
    });
  });
}

processData().then(function (data) {
  // I have access to this data!
});

// Handling rejections
function successHandler(data) {};
function errorHandler(data) {
  // Rejected!
  console.log(data);
};

var promise = new Promis(function (resolve, reject) {
  setTimeout(reject, 1000, 'Rejected!');
});

promise.then(successHandler, errorHandler);
promise.then(errorHandler);


// Unhandled rejections
var promise = new Promise(function(resolve, reject) {
  reject('Why don\'t you implement a reject handler!');
});

promise.then(function () {
  console.log('Data has arrived! Woohoo!');
});


// Using finally()
var promise = new Promise(function(resolve, reject) {
  setTimeout(reject, 1000, 'Rejected!');
});

var p = promise.then(function (data) {
  console.log(`We got some data! ${data}`);
}, function (err) {
  console.log(`We got an error! ${err}`);
}).finally(function () {
  console.log('That\'s just something I always do!');
});


// All, example
var p1 = new Promise(function (resolve, reject) { setTimeout(resolve, 1000, 'p1'); });
var p2 = new Promise(function (resolve, reject) { setTimeout(resolve, 500, 'p2'); });
var p3 = new Promise(function (resolve, reject) { setTimeout(resolve, 2000, 'p3'); });
var p4 = new Promise(function (resolve, reject) { setTimeout(resolve, 3000, 'p4'); });

Promise.all([p1, p2, p3, p4]).then(function (values) {
  // ["p1", "p2", "p3", "p4"]
  console.log(values);
});


var p1 = new Promise(function (resolve, reject) { setTimeout(resolve, 1000, 'p1'); });
var p2 = new Promise(function (resolve, reject) { setTimeout(resolve, 500, 'p2'); });
var p3 = new Promise(function (resolve, reject) { setTimeout(reject, 2000, 'p3'); });
var p4 = new Promise(function (resolve, reject) { setTimeout(resolve, 3000, 'p4'); });

Promise.all([p1, p2, p3, p4]).then(function (values) {
  console.log(values);
},function (err) {
  // p3
  console.log(err);
});


// Race, example
var p1 = new Promise(function (resolve, reject) { setTimeout(resolve, 1000, 'p1'); });
var p2 = new Promise(function (resolve, reject) { setTimeout(resolve, 500, 'p2'); });
var p3 = new Promise(function (resolve, reject) { setTimeout(reject, 2000, 'p3'); });
var p4 = new Promise(function (resolve, reject) { setTimeout(resolve, 3000, 'p4'); });

Promise.race([p1, p2, p3, p4]).then(function (value) {
  // p2
  console.log(value);
},function (err) {
  console.log(err);
});


// Async Await
// Example 1
const getCheatCodes = () => new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('A2, B1, B3, B5, C2, D1, D2, D4, E4');
  }, 2000);
});

async function myAsyncFunction() {
  var cheatCodes = await getCheatCodes();
  console.log(cheatCodes);
};

myAsyncFunction();

// Example 2, takes 1000 ms, tekur of langan tíma
var ken = () = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('ken');
  }, 200);
});

var sent = () = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('sent');
  }, 300);
});

var me = () = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('me');
  }, 500);
});

(async function() {
  // Sequential
  var a = await ken();
  var b = await sent();
  var c = await me();

  console.log(a + b + c); // 'kensentme'
})();

// Beturumbæta, tekur 500 ms
var ken = () = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('ken');
  }, 200);
});

var sent = () = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('sent');
  }, 300);
});

var me = () = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('me');
  }, 500);
});

(async function() {
  // In parallel
  var all = await.Promise.all([ ken(), sent(), me() ]); // [ 'ken', 'sent', 'me' ]
  console.log(all.join('')); // 'kensentme'
})();


// Example 3
async function giveMeData() {
  return 'Data!';
};

var data = giveMeData();
console.log(data); // [object Promise]


// Example 4
async function giveMeData() {
  return 'Data!';
};

giveMeData().then(data => {
  // Process data..
  console.log(data); // 'Data!'
});


// Example 5
async function giveMeData() {
  return 'Data!';
};

(async function() {
  var data = await giveMeData();
  console.log(data); // 'Data!'
})();


// Example 6
const moreThanTen = x => new Promise((resolve, reject) => {
  x > 10 ? resolve('Correct!') : reject('Incorrect!');
});

const testValue = async x => {
  try {
    var answer = await moreThanTen(x);
    console.log(answer);
  } catch (err) {
    console.log(err);
  }
};

testValue(12); // Correct!
testValue(11); // Correct!
testValue(13); // Correct!
testValue(9); // Incorrect!



















//
