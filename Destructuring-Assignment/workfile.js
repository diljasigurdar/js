
// Basic
const arr = [ 1, 2, 3, 4 ];
const [ x, y, z, m ] = arr;
console.log(x, y, z, m); // 1 2 3 4

// Default values
const [ a, b = 20 ] = [ 1 ];
console.log(a, b); // 1 20

// Rest of values
const [a, ...b ] = [1, 2, 3, 4];
console.log(a, b); // 1 [ 2, 3, 4 ]


// Object Destructuring
const human = { tag: '00000000001', species: 'human'};
const { tag, species } = human;
console.log(tag, species); // 00000000001 human

const { x: xCoord, y: yCoord } = { x: 1, y: 2};
console.log(xCoord, yCoord); // 1 2

const human = { tag: '00000000001', species: 'human'};
function printHuman({ tag, species }) {
  console.log(`Tag: ${tag}`); // Tag: 00000000001
  console.log(`Species: ${species.toUpperCase()}`); // Species: HUMAN
};

printHuman(human);


// Advanced Usage

const humans = [
  { tag: '00000000001', species: 'human'},
  { tag: '00000000002', species: 'human'}
]

for (const { tag, species } of humans) {
  console.log(`Tag: ${tag}`);
  console.log(`Species: ${species.toUpperCase()}`);
}

// Tag: 00000000001
// Species: HUMAN 
// Tag: 00000000002
// Species: HUMAN


const humanMap = {
  '00000000001': { tag: '00000000001', species: 'human'},
  '00000000002': { tag: '00000000002', species: 'human'}
};

Object.keys(humanMap).map(key => {
  const { [key]: { tag, species } } = humanMap;
  console.log(`Tag: ${tag}`);
  console.log(`Species: ${species.toUpperCase()}`);
});

// Tag: 00000000001
// Species: HUMAN
// Tag: 00000000002
// Species: HUMAN












//
