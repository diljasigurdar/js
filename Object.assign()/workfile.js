
// two arguments
const tea = {};
const withSugar = { sugar: false };
Object.assign(tea, withSugar);
console.log(tea); // { sugar: false }

// three arguments
const tea2 = { brand: 'Earl Gray' };
const withSugar2 = { sugar: true };
const newTea = Object.assign(tea2, withSugar2, { brand: 'Melrose' });
console.log(newTea); // { brand: 'Melrose', sugar: true }


const monsterObject = Object.assign({},
  { x: 1 },
  { y: 2 },
  { z: 3 },
  { m: 4 },
  { n: 5 },
  { i: 6 },
  { j: 7 },
  { h: 8 });
console.log(monsterObject); // { x: 1, y: 2, z: 3, m: 4, n: 5, i: 6, j: 7, h: 8 }











//
