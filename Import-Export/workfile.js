



// Import the named exports fpr divide and multiply
import { divide, multiply } from './math.js';

// Import the default export for Math
import Math from './math.js';

// Console log the default export
console.log(Math);

// Use the functions to perform some math operations
console.log(divide(4, 2));
console.log(multiply(100, 100));
console.log(Math.sum(1, 2, 3, 4, 5, 6, 7, 8));







//
