


// Export a named export called divide
export const divide = (a, b) => a / b;

// Export a named export called multiply
export const multiply = (a, b) => a * b;

// Export a default export which should be an object containing the property sum, which is a function which accepts unlimited
// arguments which will all be summed together
export default {
  sum: function() {
    return Object.values(arguments).reduce((acc, elem) =>
    acc + elem, 0);
  }
};
