/*
    Array.map() exercises
*/

var arrOne = [1, 2, 3, 4];
var arrTwo = ['a', 'b', 'c', 'd'];
var arrThree = [
  {
    id: 1,
    name: 'Harry'
  },
  {
    id: 2,
    name: 'Mr. Magoo'
  },
  {
    id: 3,
    name: 'Fred'
  }
];

// Create a new array out of arrOne where all elements are squared
const squaredArrOne = arrOne.map(elem => elem * elem);
console.log(squaredArrOne);

// Create a new array out of arrOne where all the elements are divided by 2 using a named function

function divideByTwo(number) {
  return number / 2;
}

const dividedArrOne = arrOne.map(divideByTwo);
console.log(dividedArrOne);

// Create a new array out of arrTwo where each letter is turned to uppercase

const uppercaseArrTwo = arrTwo.map(elem => elem.toUpperCase());
console.log(uppercaseArrTwo);

// Create a new array out of arrTwo where if the index is an even numbber it should use the combination of the current letter with
// the next letter, otherwise the combination of the current letter with the letter before

const combinationArr = arrTwo.map((elem, index, arr) => {
  if (index % 2 == 0) {
    return elem + arr[index + 1]; //ab
  }
  return elem + arr[index - 1]; //ba
});
console.log(combinationArr);

// Create a new array out of arrThree where each objects properties have changed: id => identifier, name => firstName, but the
// values stay the same

const modifiedPersons = arrThree.map(elem => {
  return {
    identifier: elem.id,
    firstName: elem.name
  };
});
console.log(modifiedPersons);

// Modify the arrThree array by removing all items with an odd numbered id

arrThree.map((elem, index, arr) => {
  if (elem.id % 2 !== 0) { arr.splice(index, 1); }
});

console.log(arrThree );
