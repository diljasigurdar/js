
const lessThanTwo = item => item < 2;

const ssns = [ '111111-0000', '111111-0001', '111111-000A', '11111100000'];
const ssnRegex = /^[0-9]{6}-?[0-9]{4}$/;

const numbers = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const customers = [
  {
    id: 1,
    nationality: 'US',
    name: 'John Smith'
  },
  {
    id: 2,
    nationality: 'CA',
    name: 'Lisa Dorth'
  },
  {
    id: 3,
    nationality: 'IS',
    name: 'Margrét Gunnarsdóttir'
  },
  {
    id: 4,
    nationality: 'US',
    name: 'Fred Tilburry'
  }
];
const words = [ 'someone', 'green', 'tomato', 'wholesome', 'morning', 'somebody'];

// Create a new array woth all the valid ssn's in the list above

const validSsns = ssns.filter(ssn => ssnRegex.test(ssn));
console.log(validSsns);

// Create a new array with all ssn's which are invalid

const invalidSsns = ssns.filter(ssn => !ssnRegex.test(ssn));
console.log(invalidSsns);

// Use the lessThanTwo function to get all numbers less than two

const numArr = numbers.filter(lessThanTwo);
console.log(numArr);

// Filter all customers which are from the Unites States

const usCustomers = customers.filter(customer => customer.nationality === 'US');
console.log(usCustomers);

// Delete all elements which have the word 'some' as a substring within them and return all the words which were deleted

const deletedWords = words.filter((word, index, arr) => {
  if (word.indexOf('some') !== -1) {
    // remove element
    arr.splice(index, 1);
    return true;
  }
  return false;
});
console.log(deletedWords);
console.log(words);











// hello
