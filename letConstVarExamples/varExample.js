

function scope() {
  x = 20;
  var z = 10;
}

scope();

console.log(x); // 20
console.log(z); // ReferenceError: z is not defined
