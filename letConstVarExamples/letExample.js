

function scope() {
  let x = 10;
  if (x == 10) {
    let y = 20;
    console.log(y); // 20
  }
  console.log(x); // 10
  console.log(y); // ReferenceError: y is not defined
}

scope();
