

function scope() {
  const x = 10;
  if (x == 10) {
    const y = { message: 'hello' };
    console.log(y); // { message: 'hello' };
    y.message = 'hola';
    console.log(y); // { message: 'hola' };
    y = { message: 'ciao!' }; // TypeError: Assigment to constant variable.
  }

  console.log(x); // 10
  console.log(y); // ReferenceError: y is not defined
}

scope();
