


const numbers = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ];
const words = [ 'Hello', 'my', 'name', 'is', 'Mr.', 'Bobby' ];
const duplicates = [ 's', 's', 1, 1, 2, 2, 2, 4, 6, 7, 'nice', 2, 4, 6, 3, 'nice' ];


// Count all elements withing an array

const count = numbers.reduce((accumulator, element) => accumulator + 1, 0);
console.log(count);

// Sum all numvers withing an array

const sumAllNumbers = numbers.reduce((acc, elem) => acc + elem, 0);
console.log(sumAllNumbers);

// Concatenate all the words into a sentence

const sentence = words.reduce((acc, elem) => acc + ' ' + elem, '');
console.log(sentence.trim());

// Reverse the sentence

const reverseSentence = words.reduce((acc, elem) => elem + ' ' + acc, '');
const reverseSentence2 = words.reduceRight((acc, elem) => acc + ' ' + elem, '')
console.log(reverseSentence);
console.log(reverseSentence2.trim());

// Remove all duplicates from duplicates array

const set = duplicates.reduce((acc, elem) => {
    if (acc.indexOf(elem) === -1) {
      acc.push(elem);
    }
    return acc;
}, []);
set.sort();
console.log(set);
